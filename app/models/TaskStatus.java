package models;

import javax.persistence.*;

import com.avaje.ebean.Model;

@Entity
@Table(name = "task_status_tbl")
public class TaskStatus extends Model {
	@Id
	@Column(name = "status_id")
	public Integer id;
	
	@Column(name = "status", columnDefinition = "varchar(45) not null")
	public String status;
	
	public Integer getId() {
		return id;
	}
}
