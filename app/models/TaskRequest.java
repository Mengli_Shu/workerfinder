package models;

import javax.persistence.*;

import com.avaje.ebean.Model;

import java.util.*;

@Entity
@Table(name = "task_request_tbl")
public class TaskRequest extends Model {
	@Id
	@Column(name = "task_request_id")
	public Long id;
	
	@Column(name = "title", columnDefinition = "varchar(255) not null")
	public String title;
	
	@Column(name = "description", columnDefinition = "not null")
	public String description;
	
	@Column(name = "budget", columnDefinition = "decimal(7,2) not null")
	public Double budget;
	
	@Column(name = "deadline", columnDefinition = "not null")
	public Calendar deadline;
	
	@Column(name = "num_workers", columnDefinition = "not null")
	public Integer numWorkers;
	
	@Column(name = "request_time", columnDefinition = "not null")
	public Calendar requestTime;
	
	@Column(name = "start_time", columnDefinition = "not null")
	public Calendar startTime;
	
	@Column(name = "end_time")
	public Calendar endTime;
	
	@Column(name = "hour_per_week")
	public Integer hourPerWeek;
	
	@OneToOne
	@Column(name = "task_status_tbl_status_id", columnDefinition = "not null")
	public TaskStatus status;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
