package models;

import javax.persistence.*;
import com.avaje.ebean.Model;
//import java.util.*;

@Entity
@Table(name = "task_history")
public class TaskHistory extends Model {
	@Id
	@Column(name = "history_id")
	public Long id;
	
	@OneToOne
	@Column(name = "task_request_tbl_task_request_id", columnDefinition = "not null")
	public TaskRequest taskrequest;
	
	@OneToOne
	@Column(name = "user_tbl_asworker_id")
	public User worker;
	
	@OneToOne
	@Column(name = "payment_review_tbl_payment_id")
	public PaymentReview paymentReview;
	
	// Getter and Setter
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
