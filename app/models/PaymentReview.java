package models;

import javax.persistence.*;

import com.avaje.ebean.Model;

import java.util.*;

@Entity
@Table(name = "payment_review_tbl")
public class PaymentReview extends Model {
	@Id
	@Column(name = "payment_id")
	public Long id;
	
	@Column(name = "agreed_payment", columnDefinition = "decimal(7,2) not null")
	public Double agreedPayment;
	
	@Column(name = "actual_payment")
	public Double actualPayment;
	
	@Column(name = "credibility_to_worker")
	public Double credibilityToWorker;
	
	@Column(name = "review_content")
	public String review;
	
	@Column(name = "date")
	public Calendar date;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
