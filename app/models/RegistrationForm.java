package models;

import java.util.*;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
//import play.data.validation.Validation;

public class RegistrationForm {
	
	@Constraints.Required
	public String firstName;
	
	@Constraints.Required
	public String lastName;
	
	@Constraints.Required
	public String password;
	
	@Constraints.Required
	public String confirmPassword;
	
	@Constraints.Required
	@Constraints.Email
	public String email;
	
	@Constraints.Required
	public String gender;
	
	@Constraints.Required
	public String cellNum;
	
	public String address;
	
	@Constraints.Required
	public String city;
	
	@Constraints.Required
	public String province;
	
	@Constraints.Required
	public String zip;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public String getCellNum() {
		return cellNum;
	}
	public void setCellNum(String cellNum) {
		this.cellNum = cellNum;
	}
	
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();		
		
		if(!password.equals(confirmPassword)){
			errors.add(new ValidationError("confirmPassword", "Passwords do not match."));
		}
		
		if(cellNum.length() != 10){
			errors.add(new ValidationError("cellNum", "Please provide valid phone number."));
		}

		if(zip.length() != 6){
			errors.add(new ValidationError("zip", "Please provide valid zip code"));
		}
		
		return errors.isEmpty() ? null : errors;
	}
}
