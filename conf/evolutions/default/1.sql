# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table authority_tbl (
  authority_id              integer auto_increment not null,
  authority                 varchar(45) not null,
  constraint pk_authority_tbl primary key (authority_id))
;

create table city_tbl (
  city_id                   bigint auto_increment not null,
  name                      varchar(255) not null,
  province_province_id      bigint,
  constraint pk_city_tbl primary key (city_id))
;

create table gender_tbl (
  gender_id                 integer auto_increment not null,
  gender                    varchar(5) not null,
  constraint pk_gender_tbl primary key (gender_id))
;

create table payment_review_tbl (
  payment_id                bigint auto_increment not null,
  agreed_payment            decimal(7,2) not null,
  actual_payment            double,
  credibility_to_worker     double,
  review_content            varchar(255),
  date                      datetime(6),
  constraint pk_payment_review_tbl primary key (payment_id))
;

create table province_tbl (
  province_id               bigint auto_increment not null,
  name                      varchar(255) not null,
  constraint pk_province_tbl primary key (province_id))
;

create table task_history (
  history_id                bigint auto_increment not null,
  taskrequest_task_request_id bigint,
  worker_user_id            bigint,
  payment_review_payment_id bigint,
  constraint uq_task_history_taskrequest_task_request_id unique (taskrequest_task_request_id),
  constraint uq_task_history_worker_user_id unique (worker_user_id),
  constraint uq_task_history_payment_review_payment_id unique (payment_review_payment_id),
  constraint pk_task_history primary key (history_id))
;

create table task_request_tbl (
  task_request_id           bigint auto_increment not null,
  title                     varchar(255) not null,
  description               not null,
  budget                    decimal(7,2) not null,
  deadline                  not null,
  num_workers               not null,
  request_time              not null,
  start_time                not null,
  end_time                  datetime(6),
  hour_per_week             integer,
  status_status_id          integer,
  constraint uq_task_request_tbl_status_status_id unique (status_status_id),
  constraint pk_task_request_tbl primary key (task_request_id))
;

create table task_status_tbl (
  status_id                 integer auto_increment not null,
  status                    varchar(45) not null,
  constraint pk_task_status_tbl primary key (status_id))
;

create table user_tbl (
  user_id                   bigint auto_increment not null,
  email                     varchar(320) not null unique,
  first_name                varchar(255) not null,
  last_name                 varchar(255) not null,
  password                  varchar(32) not null,
  cell                      varchar(32) not null unique,
  address                   varchar(255) not null,
  zipcode                   varchar(45) not null,
  credibility               decimal(3,2) not null default 0.5,
  city_city_id              bigint,
  gender_gender_id          integer,
  role_role_id              integer,
  authority_authority_id    integer,
  constraint uq_user_tbl_gender_gender_id unique (gender_gender_id),
  constraint uq_user_tbl_role_role_id unique (role_role_id),
  constraint uq_user_tbl_authority_authority_id unique (authority_authority_id),
  constraint pk_user_tbl primary key (user_id))
;

create table user_role_tbl (
  role_id                   integer auto_increment not null,
  role_name                 varchar(45) not null,
  constraint pk_user_role_tbl primary key (role_id))
;

alter table city_tbl add constraint fk_city_tbl_province_1 foreign key (province_province_id) references province_tbl (province_id) on delete restrict on update restrict;
create index ix_city_tbl_province_1 on city_tbl (province_province_id);
alter table task_history add constraint fk_task_history_taskrequest_2 foreign key (taskrequest_task_request_id) references task_request_tbl (task_request_id) on delete restrict on update restrict;
create index ix_task_history_taskrequest_2 on task_history (taskrequest_task_request_id);
alter table task_history add constraint fk_task_history_worker_3 foreign key (worker_user_id) references user_tbl (user_id) on delete restrict on update restrict;
create index ix_task_history_worker_3 on task_history (worker_user_id);
alter table task_history add constraint fk_task_history_paymentReview_4 foreign key (payment_review_payment_id) references payment_review_tbl (payment_id) on delete restrict on update restrict;
create index ix_task_history_paymentReview_4 on task_history (payment_review_payment_id);
alter table task_request_tbl add constraint fk_task_request_tbl_status_5 foreign key (status_status_id) references task_status_tbl (status_id) on delete restrict on update restrict;
create index ix_task_request_tbl_status_5 on task_request_tbl (status_status_id);
alter table user_tbl add constraint fk_user_tbl_city_6 foreign key (city_city_id) references city_tbl (city_id) on delete restrict on update restrict;
create index ix_user_tbl_city_6 on user_tbl (city_city_id);
alter table user_tbl add constraint fk_user_tbl_gender_7 foreign key (gender_gender_id) references gender_tbl (gender_id) on delete restrict on update restrict;
create index ix_user_tbl_gender_7 on user_tbl (gender_gender_id);
alter table user_tbl add constraint fk_user_tbl_role_8 foreign key (role_role_id) references user_role_tbl (role_id) on delete restrict on update restrict;
create index ix_user_tbl_role_8 on user_tbl (role_role_id);
alter table user_tbl add constraint fk_user_tbl_authority_9 foreign key (authority_authority_id) references authority_tbl (authority_id) on delete restrict on update restrict;
create index ix_user_tbl_authority_9 on user_tbl (authority_authority_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table authority_tbl;

drop table city_tbl;

drop table gender_tbl;

drop table payment_review_tbl;

drop table province_tbl;

drop table task_history;

drop table task_request_tbl;

drop table task_status_tbl;

drop table user_tbl;

drop table user_role_tbl;

SET FOREIGN_KEY_CHECKS=1;

