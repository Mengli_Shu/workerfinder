package models;

//import java.util.*;
//import java.lang.*;
//import java.beans.*;
//import java.math.BigDecimal;
import javax.persistence.*;
import com.avaje.ebean.Model;
import play.data.validation.Constraints;

@Entity
@Table(name = "user_tbl")
public class User extends Model {
	
	public static Finder<Long,User> find = new Finder<>(User.class);
	
	@Id
	@Column(name = "user_id")
	private Long id;
	
	@Column(name = "email", columnDefinition = "varchar(320) not null unique")
	public String email;
	
	@Column(name = "first_name", columnDefinition = "varchar(255) not null")
	public String firstName;
	
	@Column(name = "last_name", columnDefinition = "varchar(255) not null")
	public String lastName;
	
	@Column(name = "password", columnDefinition = "varchar(32) not null")
	private String password;
	
	@Column(name = "cell", columnDefinition = "varchar(32) not null unique")
	public String cell;
	
	@Column(name = "address", columnDefinition = "varchar(255) not null")
	public String address;
	
	@Column(name = "zipcode", columnDefinition = "varchar(45) not null")
	public String zipcode;
	
	@Column(name = "credibility", columnDefinition = "decimal(3,2) not null default 0.5")
	public Double credibility = 0.5;
	
	@ManyToOne()
	@Column(name = "city_tbl_city_id", columnDefinition = "not null")
	public City city;
	
	@OneToOne
	public Gender gender;
	
	@OneToOne
	public UserRole role;
	
	@OneToOne
	public Authority authority;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}