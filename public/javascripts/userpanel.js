/*
 * This is the js file for user profile page.
 */


$("#editProfile").click(function() {
	$(document.getElementById("edit-profile-form")).show("slow");
});

$("#cancelProfile").click(function() {
	$(document.getElementById("edit-profile-form")).hide("slow");
});


$("#changePWD").click(function() {
	$(document.getElementById("change-pwd-div")).show("slow");
});

$("#cancelpwd").click(function() {
	$(document.getElementById("change-pwd-div")).hide("slow");
});

function switchState(state){
		/*var elem = document.getElementById("workerAuth");*/
		
	
		if(state.value == "Enable") {
			state.value = "Enabled";
			state.classList.add("disabled");
		
			var counterState = document.getElementById("disableW");
			counterState.classList.remove("disabled");
			counterState.value = "Disable";
		
			/* show skills */
			var workerField = document.getElementById("worker-field");
			$(workerField).show("slow");
		}
		else if(state.value == "Disable") {
			state.value = "Disabled";
			state.classList.add("disabled");
		
			var counterState = document.getElementById("enableW");
			counterState.classList.remove("disabled");
			counterState.value = "Enable";
		
			/* hide skills */
			var workerField = document.getElementById("worker-field");
			$(workerField).hide("slow");
		}
}

$("#newSkill").click(function(){
	$(document.getElementById("skillForm")).show("slow");
});


$('#addSkill').click(function(e) {
	e.preventDefault();
	
	var entry = '<div class="skill-added"><fieldset class="form-group col-md-2"><label for="skillName">Skill</label><select class="form-control" id="skillName"><option disabled selected value> -- skill -- </option><option>Programming</option><option>Moving</option><option>Cooking</option></select></fieldset>'
				+ '<fieldset class="form-group col-md-2"><label for="skillLevel">Level</label><select class="form-control" id="skillLevel"><option disabled selected value> -- level -- </option><option>Beginner</option><option>Advanced</option><option>Expert</option></select></fieldset>'
				+ '<fieldset class="form-group col-md-2"><label for="charge">Charge</label><input type="text" class="form-control" id="charge"></fieldset>'
				+ '<fieldset class="form-group"><label for="skillDescription">Brief Description</label><textarea class="form-control" id="skillDescription" rows="2"></textarea></fieldset>';
	
	document.getElementById("skillEntry").innerHTML += entry;
});

$("#deleteSkill").click(function(e) {
	e.preventDefault();
	var lastSkill = document.getElementById("skillEntry").lastElementChild;
	$(lastSkill).remove();
});

$("#cancel-new-skill").click(function() {
	$("#skillEntry").empty();
	$(document.getElementById("skillForm")).hide("slow");
});

$("#add-required-skill").click(function(e) {
	e.preventDefault();
	var entry ='<div class="row">\r\n<fieldset class="form-group col-md-3">\r\n<label for="skills-required">Skills Required:</label>\r\n<select class="form-control" id="skills-required"></select>\r\n</fieldset>'
				+ '<fieldset class="form-group col-md-3">\r\n<label for="num-worker">Number of workers:</label>\r\n<input type="text" class="form-control" id="num-worker">\r\n</fieldset>\r\n</div>';
	document.getElementById("required-skill-entry").innerHTML += entry;
});

$("#delete-required-skill").click(function(e) {
	e.preventDefault();
	var lastRequiredSkill = document.getElementById("required-skill-entry").lastElementChild;
	$(lastRequiredSkill).remove();
});



$(document).ready(function() {
    $('#start-date-picker').datetimepicker();
    $('#end-date-picker').datetimepicker({
        useCurrent: false //Important! See issue #1075
    });
    $("#start-date-picker").on("dp.change", function (e) {
        $('#end-date-picker').data("DateTimePicker").minDate(e.date);
    });
    $("#end-date-picker").on("dp.change", function (e) {
        $('#start-date-picker').data("DateTimePicker").maxDate(e.date);
    });
});

$("#rank").click(function() {
	window.location = "ranking";
});

$("#reset-request").click(function() {
	document.getElementById("request-form").reset();
});