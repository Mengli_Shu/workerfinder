package models;

import javax.persistence.*;

import com.avaje.ebean.Model;

import java.util.*;

@Entity
@Table(name = "city_tbl")
public class City extends Model {
	@Id
	@Column(name = "city_id")
	public Long id;
	
	@Column(name = "name", columnDefinition = "varchar(255) not null")
	public String name;
	
	@OneToMany(mappedBy = "city")
	public List<User> users = new ArrayList<>();
	
	@ManyToOne()
	@Column(name = "province_tbl_province_id", columnDefinition = "not null")
	public Province province;
	
	public Long getId() {
		return id;
	}
}
