package models;

import javax.persistence.*;

import com.avaje.ebean.Model;

import java.util.*;

@Entity
@Table(name = "province_tbl")
public class Province extends Model {
	@Id
	@Column(name = "province_id")
	public Long id;
	
	@Column(name = "name", columnDefinition = "varchar(255) not null")
	public String name;
	
	@OneToMany(mappedBy = "province")
	public List<City> cities = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
}
