package models;

import javax.persistence.*;

import com.avaje.ebean.Model;

@Entity
@Table(name = "authority_tbl")
public class Authority extends Model {
	@Id
	@Column(name = "authority_id")
	public Integer id;
	
	@Column(name = "authority", columnDefinition = "varchar(45) not null")
	public String authority;
	
	public Integer getId() {
		return id;
	}
}
