package controllers;

import play.*;
import play.mvc.*;
import views.html.*;
import play.data.*;
import models.LoginForm;
import models.RegistrationForm;
import play.data.validation.Constraints;
import views.*;
//import play.data.Form;

public class Application extends Controller {
	//final static Form<LoginForm> userForm = Form.form(LoginForm.class);
	//final static Form<RegistrationForm> regiForm = Form.form(RegistrationForm.class);
	
    public Result index() {
    	Form<LoginForm> userForm = Form.form(LoginForm.class);
        return ok(index.render(userForm));
    }
    public Result signup() {
    	Form<RegistrationForm> regiForm = Form.form(RegistrationForm.class);
    	return ok(signup.render(regiForm));
    }
    public Result confirm() {
    	return ok(confirmation.render());
    }
    
    public Result authenticate() {    		
    	
    	Form<LoginForm> loginForm = Form.form(LoginForm.class).bindFromRequest();
    	if(loginForm.hasErrors()) {
    		return badRequest(views.html.index.render(loginForm));
    	}
    	else {
    		LoginForm loginData = loginForm.get();
    		return ok(authenticate.render(loginData));
    	}    	
    	
    }
    
    public Result validateForm() {
    	Form<RegistrationForm> filledRegiForm = Form.form(RegistrationForm.class).bindFromRequest();
    	
    	if(filledRegiForm.hasErrors()) {
    		return badRequest(views.html.signup.render(filledRegiForm));
    	}
    	else {
    		RegistrationForm regiData = filledRegiForm.get();
    		return ok();
    	}
    }      
    
    
    
    
    public Result profile() {
    	return ok(profile.render());
    }
    
    public Result taskhistory() {
    	return ok(taskhistory.render());
    }
    
    public Result requesthistory() {
    	return ok(requesthistory.render());
    }
    
    public Result newrequest() {
    	return ok(newrequest.render());
    }
    
    public Result deleteaccount() {
    	return ok(deleteaccount.render());
    }
    
    public Result taskpage() {
    	return ok(taskpage.render());
    }
    
    public Result profileview() {
    	return ok(profileview.render());
    }
    
    public Result ranking() {
    	return ok(ranking.render());
    }
    
    public Result forgetpwd() {
    	return ok(forgetpwd.render());
    }
}

