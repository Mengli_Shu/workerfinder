package models;

import javax.persistence.*;

import com.avaje.ebean.Model;

@Entity
@Table(name = "user_role_tbl")
public class UserRole extends Model {
	@Id
	@Column(name = "role_id")
	public Integer id;
	
	@Column(name = "role_name", columnDefinition = "varchar(45) not null")
	public String role;
	
	public Integer getId() {
		return id;
	}
}
