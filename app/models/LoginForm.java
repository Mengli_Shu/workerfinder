package models;


import play.data.validation.Constraints;

public class LoginForm {
	
	@Constraints.Required
	@Constraints.Email
	protected String email;
	
	@Constraints.Required
	protected String password;
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
	
	
}
