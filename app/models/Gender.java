package models;

//import java.util.*;
//import java.beans.*;
import javax.persistence.*;

import com.avaje.ebean.Model;

@Entity
@Table(name = "gender_tbl")
public class Gender extends Model{
	@Id
	@Column(name = "gender_id")
	public Integer id;
	
	@Column(name = "gender", columnDefinition = "varchar(5) not null")
	public String gender;
	
	public Integer getId() {
		return id;
	}
}
